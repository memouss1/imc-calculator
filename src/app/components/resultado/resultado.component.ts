import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.css']
})
export class ResultadoComponent implements OnInit {
  bmi: number;
  rsultado : string;
  interpretacion : string

  constructor( private route: ActivatedRoute ) { 
    // console.log(route.snapshot.paramMap.get('valor'));
    this.rsultado = '';
    this.interpretacion = '';
    this.bmi = +route.snapshot.paramMap.get('valor')!;
    // console.log(this.bmi);


    
  }

  ngOnInit(): void {
    this.getResultado()
  }

  getResultado(){
    if(this.bmi >= 25){
      this.rsultado = 'Exeso de Peso';
      this.interpretacion = 'Estas gordo :v ';
    }else if(this.bmi >= 18.5) {
      this.rsultado = 'Normal';
      this.interpretacion = 'Estás como San Eleno'
    } else if (this.bmi <= 18.5) {
      this.rsultado = 'Bajo de Peso';
      this.interpretacion = 'Vamos por unos tacos, si o que....'
    }
  }
}
