import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './components/inicio/inicio.component';
import { ResultadoComponent } from './components/resultado/resultado.component';
import { AlgoComponent } from './components/algo/algo.component';


const routes: Routes = [
  { path: 'home', component:  InicioComponent},
  { path: 'resultado/:valor', component:  ResultadoComponent},
  { path: 'algo', component: AlgoComponent },
  { path: '**', redirectTo: 'home' }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
